const { response, request } = require('express')
const usuariosGet = (req, res = response) => {
    res.status(403).json({
        "msj": "hola",
        "status": "get controller",
    })
}

const usuariosPost = (req, res = response) => {
    console.log(req.body);
    const body = req.body;
    const { nombre, ci, correo } = req.body
    res.status(201).json({
        "msj": "hola",
        "status": "post",
        body,
        nombre,
        ci,
        correo
    })
}

const usuariosPostVar = (req, res = response) => {
    res.status(201).json({
        "msj": "hola",
        "status": "post Var",
    })
}

const usuariosDelete = (req, res = response) => {
    res.status(200).json({
        "msj": "hola",
        "status": "delete",
    })
}

const usuariosPut = (req, res = response) => {
    res.status(400).json({
        "msj": "hola",
        "status": "put",
    })
}

const usuariosPutVar = (req, res = response) => {
    const id = req.params.id;

    res.status(200).json({
        "msj": "hola",
        "status": "put VAR",
        id,
    })
}

const usuariosPutQueryParams = (req = request, res = response) => {
    const query = req.query;
    const { q, nombre = 'No name', page = 1, limit } = req.query;

    res.status(400).json({
        "msj": "hola",
        "status": "put query params",
        query,
        q,
        page,

    })
}

const usuariosPatch = (req, res = response) => {
    res.status(200).json({
        "msj": "hola",
        "status": "patch",
    })
}

module.exports = {
    usuariosGet,
    usuariosPost,
    usuariosDelete,
    usuariosPut,
    usuariosPutVar,
    usuariosPutQueryParams,
    usuariosPatch,
    usuariosPostVar,
}