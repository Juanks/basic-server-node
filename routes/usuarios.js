const { Router } = require('express');
const { usuariosGet, usuariosPost, usuariosDelete, usuariosPut, usuariosPatch, usuariosPostVar, usuariosPutVar, usuariosPutQueryParams } = require('../controllers/usuariosController');

const router = new Router();

router.get('/', usuariosGet);
router.post('/save', usuariosPost);
router.post('/savevar', usuariosPostVar)
router.delete('/', usuariosDelete);
router.put('/', usuariosPut);
router.put('/query_params', usuariosPutQueryParams);
router.put('/actualizar/:id', usuariosPutVar)
router.patch('/', usuariosPatch);


module.exports = router