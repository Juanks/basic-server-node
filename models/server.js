const express = require('express')
const cors = require('cors')

class Server {
    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.usuariosPath = '/api/usuarios'
        //middlewares
        this.middlewares();

        //rutas de la app
        this.router()
    }

    middlewares() {
        //CORS
        this.app.use(cors());

        //lectura y parseo del bosy
        this.app.use(express.json());

        //directorio publico
        this.app.use(express.static('public'));
    }

    router() {
        this.app.use(this.usuariosPath, require('../routes/usuarios'))
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto', this.port)
        });
    }
}
module.exports = Server;